Integration web
==========================

Intégration d'une maquette dans le cadre de mes études aux Gobelins.

* Cloner le projet avec git clone
* Avoir gulp sur son ordinateur
* ouvrir un terminal à la racine du projet et executer 'npm install'
* Ouvrir un terminal et lancer 'gulp' avec la racine du projet
* Ouvrir un navigateur web et aller sur localhost:3000
* En cas de modifications et de déploiement, lancer la commande gulp deploy qui va créer un dossier src.

Je m'excuse, je suis très déçu de mon rendu.
C'est vraiment mauvais. Et des bugs n'ont pas arrangés la remise.
On peut constater que le tri ne fonctionne qu'une fois, et le slider anime mes items.
Je n'ai pas compris pourquoi. Je m'en excuse vraiment... 
Je tâcherai de ne plus refaire ces erreurs.
