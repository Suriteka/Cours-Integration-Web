class Bseller {
    constructor (el) {
        this.el = el;
        this.items = [];
        this.max = 0;
    }

    init(url){
        this.load(url);
        this.listen();        
    }

    load(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.addEventListener('readystatechange', this.loaded.bind(this, req));
        req.send();
    }

    loaded(req){
        if(req.readyState == 4){
            if(req.status == 200){
                this.build(JSON.parse(req.responseText));
            }
        }
    }

    build(datas){        
        this.datas = datas;
        this.max = datas.length;

        for(var i = 0; i < this.max; i++){            
            var item = new Bseller_item(i, datas[i]);
            item.build();
            this.el.appendChild(item.build());
            this.items.push(item);
        }
    }

    listen(){
        this.select = document.querySelector('.js__select--bseller');
        this.select.addEventListener('change', () => this.compare());
    }

    compare(){
    // Bug lorsque je reselect
       this.el.innerHTML = '';                
       this.build(this.sortItem());       
    }

    sortItem(){
        var filter = this.select.value;      
        return this.datas.sort(function (a, b) {
            if(filter == 'price'){
                if(a.price > b.price) {
                    return 1;
                } else {
                    return -1;
                }
            }else if(filter == 'color'){
                if(a.color > b.color) {
                    return 1;
                } else {
                    return -1;
                }
            }else if(filter == 'price'){
                if(a.price > b.price) {
                    return 1;
                } else {
                    return -1;
                }
            }
        })
    }

}

// Class 
class Bseller_item {
    constructor (id, data) {
        this.id = id;        
        this.data = data;
    }

    build(){
        this.el = document.createElement('li');
        this.el.classList.add('bseller__item');

        this.el.id = this.data.id;

        if(this.data.stock < 2) this.el.classList.add('bseller__item--urgent');
        
        this.el.innerHTML = 
        '<article>' + 
            '<header class="bs-item__header">' +
                '<img src="' + this.data.img + '">' +
                '<h1>' + this.data.title + '</h1>' +
            '</header>' +
            '<p class="bs-item__desc">' + this.data.model + ' - ' + this.data.color + '</p>' +
            '<footer>' +
                '<span class="bseller__stock">' + this.data.stock + ' en stock</span>' +
            '</footer>' +
        '</article>';

        return this.el;
    }
}

var bseller = new Bseller(document.querySelector('.bseller__items'));
bseller.init('scripts/bseller.json');