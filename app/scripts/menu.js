var hamburger = this.document.querySelector('.hamburger');

hamburger.addEventListener('click', function(){
    document.querySelector('.mobile__links').classList.toggle('mobile__links--open');
    hamburger.querySelector('.button__hamburger').classList.toggle('button__hamburger--close');
    document.querySelector('html').classList.toggle('overflow--hiden');
});