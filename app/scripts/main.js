class Slider {
    constructor(el){
        this.el = el;
        this.items = [];
        this.current = 0;
        this.max = 0;
    }

    init(url){
        this.container = this.el.querySelector('.slider__list');
        this.left = document.querySelector('.left-arrow');
        this.right = document.querySelector('.right-arrow');
    
        this.load(url);
        // Lance l'écoute
        this.listen();
    }

    load(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.addEventListener('readystatechange', this.loaded.bind(this, req));
        req.send();
    };

    loaded(req){
        if (req.readyState == 4){
            if(req.status == 200){
                this.build(JSON.parse(req.responseText));
            }
        }
    }

    build(datas){
        this.max = datas.length;
        var bullets = this.el.querySelector('.slider__controls');
        //this.bullets = new SliderBullets(this.el.querySelector('.slider__controls'));
    
        var i = 0;
        var item;
        var bullet;
    
        // Petite boucle qui coupe chacun de nos élements du JSON et envoie dans bullets et rajoute un item dans notre element.
        for(i; i < this.max; i++){
            item = new SliderItem(i, datas[i]);
            this.el.appendChild(item.build());
    
            this.items.push(item);
            //bullet = new SliderBullet(i, datas[i]);
            //bullets.appendChild(bullet.build());
            //this.bullets.add(i, datas[i]);//
        }
        this.changeCurrent(this.current);
    }

    changeCurrent(id){
        var active = document.querySelector('.slide__item--active');
        var future_active = document.getElementById(id);
    
        if(active != null){
            active.classList.remove('slide__item--active');
        }
        future_active.classList.add('slide__item--active');
    }

    listen(){
        this.left.addEventListener('click', this.move.bind(this, -1));
        this.right.addEventListener('click', this.move.bind(this, 1));
    }

    move(dir){
        var active = document.querySelector('.slide__item--active');
        var futur_active = parseInt(active.id)+dir;
    
        TweenLite.fromTo(active, 0.5, {
            autoAlpha: 1,        
            y: 0,
        },
        {
            autoAlpha: 0.0001,        
            y: -50,
        }
        );
    
    
        if(futur_active == this.items.length){
            futur_active = 0;
        } else if(futur_active == -1) {
            futur_active = this.items.length-1;
        }
    
        this.changeCurrent(futur_active);
    
        active = document.querySelector('.slide__item--active');
        
        TweenLite.fromTo(active, 0.8, {
                autoAlpha: 0.001,        
                y: 80,
            },
            {
                autoAlpha: 1,                
                y: 0,
                ease: Power2.easeInOut                               
            }
        );
    
        TweenLite.from('.newitems__content__text', 1.4, {
            autoAlpha: 0,
            y: 80,
            ease: Power3.easeInOut                           
        });
    }

}

class SliderItem{
    constructor(id, datas){
        this.id = id;
        this.datas = datas;
    }
    
    build(){
        this.el = document.createElement('article');
        this.el.id = this.datas.id;
    
        // Création de l'élement HTML avec les différentes données pour le slider
        // Méthode un peu sale pour l'instant — TODO: améliorer la lisibilité du code
        this.el.innerHTML =
            '<img class="newitems__content__image" src="' + this.datas.img + '">' +
            '<div class="newitems__content__text"><h1>'+this.datas.title+'</h1>'
            + '<p>' + this.datas.desc + '</p>' +
            '<button class="black"><a href="' + this.datas.url + '">consulter</a></button>' + '</div>';
    
        return this.el;
    };   
}


var slider = new Slider(document.querySelector('.slider__list'));
slider.init('scripts/slider.json');
